package main

import "fmt"

func main() {
	var totalRow, choose int
	fmt.Println("=== Program Cetak Piramid ===")
	fmt.Println("Pilihan: ")
	fmt.Println("1.Tidak Terbalik")
	fmt.Println("2.Terbalik")
	fmt.Println("Pilih: ")
	fmt.Scan(&choose)
	fmt.Println("Input Row: ")
	fmt.Scan(&totalRow)

	switch choose {
	case 1:
		for row := 1; row <= totalRow; row++ {
			for space := 1; space <= (totalRow - row); space++ {
				fmt.Print(" ")
			}
			for symbol := 1; symbol <= ((2 * row) - 1); symbol++ {
				fmt.Print("*")
			}
			fmt.Println()
		}
	case 2:
		for row := 1; row <= totalRow; row++ {
			for space := 1; space < row; space++ {
				fmt.Print(" ")
			}
			for symbol := 1; symbol <= (totalRow*2 - ((2 * row) - 1)); symbol++ {
				fmt.Print("*")
			}
			fmt.Println()
		}
	default:
		fmt.Println("Pilihan tidak tersedia")
	}

	fmt.Println("\nProgram berakhir...")
}
